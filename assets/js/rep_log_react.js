import React from 'react';
//import ReactDom from 'react-dom';
import {render} from 'react-dom';  //we only import the render function form react-dom not the whole thing. it changes the sintax too we use render() instead of ReactDom.render()

//const el = React.createElement('h2', null, 'Lift History!');
//const el = React.createElement('h2', null, 'Lift History!', React.createElement('span',null, '<3'));

const shouldShowHeart = true;

import RepLogApp from './RepLog/RepLogApp';

console.log(window.REP_LOG_APP_PROPS.itemOptions);

render(
    <RepLogApp 
        withHeart={shouldShowHeart}
        {...window.REP_LOG_APP_PROPS}
    />
    , document.getElementById('lift-stuff-app')
);
