import React, {Component} from 'react';
import RepLogs from './RepLogs';
import PropTypes from 'prop-types';
import uuid from 'uuid/v4';
import {getRepLogs, deleteRepLog, createReplog} from '../api/rep_log_api'

export default class RepLogApp extends Component {

    constructor(props) {
        super(props);  //execute the parent constructor

        this.state = {
            highlightedRowId: null,
            repLogs: [],
            numberOfHearts: 1,
            isLoaded: false,
            isSavingNewRepLog: false,
            successMessage: '',
            newRepLogValidationMessage: '',
        };
        this.successMessageTimeoutHandle = 0;

        this.handleRowClick = this.handleRowClick.bind(this);
        this.handleAddRepLog = this.handleAddRepLog.bind(this);
        this.handleHeartChange = this.handleHeartChange.bind(this);
        this.handleDeleteRepLog = this.handleDeleteRepLog.bind(this);
    }

    componentDidMount() {
        getRepLogs()
        .then((data) => {
            this.setState({
                repLogs: data,
                isLoaded: true
            });
        });
    }

    componentWillUnmount() {
        clearTimeout(this.successMessageTimeoutHandle);
    }

    handleRowClick(repLogId) {
        this.setState({highlightedRowId: repLogId});
    }

    handleAddRepLog(item, reps) {
        const newRep = {
            reps: reps,
            item: item,
        };

        this.setState({isSavingNewRepLog: true});

        // wait for ajax response 
        createReplog(newRep)
            .then(repLog => { 
                this.setState(prevState => {
                    const newRepLogs = [...prevState.repLogs, repLog];
                    return {
                        repLogs: newRepLogs,
                        isSavingNewRepLog: false,
                        newRepLogValidationMessage: ''
                    };
                });
                this.setSuccessMessage('Rep Log saved!');

            }).catch(error => {
                error.response.json().then(errorsData => {
                    const errors = errorsData.errors;
                    const firstError = errors[Object.keys(errors)[0]];

                    this.setState({
                        newRepLogValidationMessage: firstError,
                        isSavingNewRepLog: false,
                    });
                })
            })
    }

    setSuccessMessage(message) {
        this.setState({
            successMessage: message
        });
        clearTimeout(this.successMessageTimeoutHandle);
        this.successMessageTimeoutHandle = setTimeout(() => {
            this.setState({
                successMessage: ''
            });
            this.successMessageTimeoutHandle = 0;
        }, 3000)
    }

    handleHeartChange(heartCount) {
        this.setState({
            numberOfHearts: heartCount
        });
    }

    handleDeleteRepLog(id) {

        deleteRepLog(id)
            .then(() => {
                this.setSuccessMessage('Item deleted');
            });

        // remove the rep log without mutating state...filter returns a new array
        this.setState((prevState) => {
            return {
                repLogs: prevState.repLogs.filter(repLog => repLog.id !== id)
            };
        });
    }

    render() {  // render runs each time the state changes, it updates the dom as needed 

        // const repLogElements = repLogs.map((repLog) => {
        //     return (
        //         <tr key={repLog.id}>
        //             <td>{repLog.itemLabel}</td>
        //             <td>{repLog.reps}</td>
        //             <td>{repLog.totalWeightLifted}</td>
        //             <td>...</td>
        //         </tr>
        //     );
        // });

        return (
            <RepLogs
                {...this.props}
                {...this.state}
                onRowClick={this.handleRowClick}
                onAddRepLog={this.handleAddRepLog}
                onHeartChange={this.handleHeartChange}
                onDeleteRepLog={this.handleDeleteRepLog}
            />
        )
    }
}

RepLogApp.propTypes = {
    withHeart: PropTypes.bool,
    itemOptions: PropTypes.array,
}